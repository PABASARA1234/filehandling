#include <stdio.h>
#include <conio.h>
#include <string.h>

int main(){

    FILE *xx;
    char text[200] = "UCSC is one of the leading institutes in Sri Lanka for computing studies.";
    int length = strlen(text);
    int n;


    xx=fopen("assignment9.txt", "w");

    if (xx==NULL)
    {
        printf("File creation failed");
    }else{
        for (n = 0; n < length; n++)
        {
            printf(" %c ", text[n]);
                fputc(text[n],xx);
        }

        printf("\n");
        printf("All the characters written successfully\n\n");
        fclose(xx);
    }


    printf("\n\n");
    printf("***Reading the final document***\n\n");

    xx=fopen("assignment9.txt", "r");

    fscanf(xx, "%[^\n]s", text);
    printf("%s", text);
    fclose(xx);

    printf("\n\n");
    printf("***Reading the final document successfull.\n\n");

    printf("\n\n");
    printf("***Appending new characters***\n\n");

    char text2[200] = "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.";
    int length2 = strlen(text2);
    int n2;

    xx=fopen("assignment9.txt", "a");

    if (xx==NULL)
    {
        printf("File appending failed");
    }else{
        for (n2 = 0; n2 < length2; n2++)
        {
            printf("  %c ", text2[n2]);
                fputc(text2[n2],xx);
        }

        printf("\n");
        printf("***All the characters appended successfully***\n\n");
        fclose(xx);
    }

    getch();
    return 0;

}

